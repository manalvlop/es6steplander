import '../styles/index.scss';
import {validateStep, validateInput} from './validator';

//=======================================SIMPLE VARIABLES

// Controls execution only on load
let loaded   = false;
// Get all steps
let steps    = document.getElementsByClassName('step');
// Get all inputs
let inputs   = document.querySelectorAll('input, select');
// Get active inputs. Defined AFTER active class has been added to step
let active_inputs;
// Get nav buttons
let prev_btn = document.getElementById('prev-btn');
let next_btn = document.getElementById('next-btn');

//=======================================SET STEPS ACCORDING TO LOCAL STORAGE

// If first time user, set steps_position to 0 else getitem from steps_position
if (isNaN(parseInt(localStorage.getItem('steps_position'))) || localStorage.getItem('steps_position') < 0 || localStorage.getItem('steps_position') > steps.length - 1) {
    localStorage.setItem('steps_position', 0);
    window.history.replaceState(0, "step_history");
} else {
    localStorage.getItem('steps_position');
}

//=======================================OBJECTS / CONSTRUCTORS

// Create counter object to be monitored by proxy
let stepState = {
    counter: parseInt(localStorage.getItem('steps_position')),
};

// Create proxy to listen to changes in counter object
let stepStateProxy = new Proxy(stepState, {
    set: function (target, prop, value) {
        active_inputs = document.querySelectorAll('.step.active input, .step.active select');
        // Store values from valid active inputs in localstorage BEFORE updating counter
        // Condition implies moving forward
        if (target[prop] < value) {
            for (let i = 0; i < active_inputs.length; i++) {
                if (active_inputs[i].type === "radio") {
                    active_inputs[i].checked ? localStorage.setItem(active_inputs[i].name, active_inputs[i].value) : null;
                } else {
                    active_inputs[i].value ? localStorage.setItem(active_inputs[i].name, active_inputs[i].value) : null;
                }
            }
        }
        // Update value (counter number) THEN dispatch custom event
        target[prop] = value;
        document.dispatchEvent(counterEvent);
        return true;
    }
});
// Custom event to be executed on counter change
let counterEvent   = new Event('onStepChange');

//=======================================EVENTS
// Listen for the counter change event
document.addEventListener('onStepChange', function () {
    if (!loaded) {
        loaded = true;
        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].type === "radio" && inputs[i].value === localStorage.getItem(inputs[i].name)) {
                inputs[i].checked = true;
            } else if (inputs[i].type === "select-one") {
                localStorage.getItem(inputs[i].name) ? inputs[i].value = localStorage.getItem(inputs[i].name) : inputs[i].value = "";
            } else if (inputs[i].type === "email" || inputs[i].type === "text") {
                inputs[i].value = localStorage.getItem(inputs[i].name);
            }
        }
    }
    // Store steps position
    localStorage.setItem('steps_position', stepStateProxy.counter);
    // Remove active class from all of the steps
    for (let i = 0; i < steps.length; i++) {
        steps[i].classList.remove('active');
    }
    // Add active class to current step
    steps[stepStateProxy.counter].classList.add('active');
    // Show/hide next and prev buttons after the first two steps
    if (stepStateProxy.counter + 1 === steps.length || stepStateProxy.counter < 2) {
        next_btn.classList.remove('active');
    } else {
        next_btn.classList.add('active');
    }
    if (stepStateProxy.counter === 0 || stepStateProxy.counter < 2) {
        prev_btn.classList.remove('active');
    } else {
        prev_btn.classList.add('active');
    }

});

// Next and Previous button functionality
prev_btn.addEventListener('click', function () {
    if (stepStateProxy.counter > -1) {
        stepStateProxy.counter--;
        window.history.pushState(stepStateProxy.counter, "step_history");
    }
});
next_btn.addEventListener('click', function () {
    if ((stepStateProxy.counter < steps.length)) {
        if (validateStep()) {
            stepStateProxy.counter++;
            window.history.pushState(stepStateProxy.counter, "step_history");
        }
    }
});

// On option select and on radio change event
document.addEventListener('click', function (e) {
    if (e.target.type === "select-one" || e.target.type === "radio") {

        stepStateProxy.counter++;
        window.history.pushState(stepStateProxy.counter, "step_history");
    }
});

//=======================================UPDATE APP ON LOAD
document.dispatchEvent(counterEvent);

//=======================================UPDATE APP ON BACK / FORWARD BROWSER BUTTONS
window.onpopstate = function (event) {
    stepStateProxy.counter = event.state;
};

active_inputs = document.querySelectorAll('.step.active input, .step.active select');
for (let i = 0; i < active_inputs.length; i++){
    active_inputs[i].addEventListener('blur', function(){
        validateStep();
    });
}