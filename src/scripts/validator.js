export let validateStep = () => {
    let active_inputs   = document.querySelectorAll('.step.active input');
    let step_checkpoint = [];
    let i;

    let invalid = (e) => {
        let thisel = e.target;
        let newNode = document.createElement('div');
        if (document.getElementById(thisel.name + "-error")) {
            document.getElementById(thisel.name + "-error").remove();
        }
        thisel.parentNode.insertBefore(newNode, document.querySelectorAll("input[name='" + thisel.name + "']").nextSibling).innerHTML = thisel.validationMessage;
        newNode.id = thisel.name + "-error";
        newNode.classList.add('error-msg');
    };

    for (i = 0; i < active_inputs.length; i++) {
        active_inputs[i].oninvalid = invalid;
        let thisel = active_inputs[i];
        if (document.getElementById(thisel.name + "-error")) {
            document.getElementById(thisel.name + "-error").remove();
        }

        step_checkpoint.push(active_inputs[i].checkValidity());
    }
    return !step_checkpoint.includes(false);
};